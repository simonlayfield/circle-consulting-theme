# Circle Consulting Wordpress Theme

### Compiling Assets

There's a gulpfile that contains a task for compiling theme SASS.
Run `gulp css` to compile, or `gulp watch` to compile as file changes are made.

There's also a task to move Bootstrap js over to the public directory, to be used if Bootstrap is updated (`gulp copy-js`).

### Gutenberg Editor Plugins

Within the theme there's a plugins dir. At the beginning of the project I was inclinced to create custom plugins to be used within the Gutenberg editor, but this proved to be a direction that was challenging to manage and achieve the level of control we needed and so is largely abandoned. A couple of these plugins are/were used for editing page content, so unless there's a reason to remove them then they're probably best kept as they are. Plugins in this directory would need to be moved over to `wp-content/themes` to be installed. This dir was kept in case this approach was revisited.

### Other Required Plugins

At the time mf writing, the plugins used on the theme/site are:

- Advanced Custom Fields Pro
- Everest Forms / Everest Forms Pro
- Simple Page Ordering
- Updraft Plus (Backups)
- Wordfence (Security)
- Wordpress SEO
- WP GDPR Compliance
- WP Job Manager
- WP Job Manager Applications
- WP Job Manager Extra Fields
- Yikes Inc Easy Mailchimp Extender

Some of these plugins (Job Manager, for instance) are integral for the site to function as intended; others aren't essential to it's function but provide admin functionality (e.g. Backups).

ACF Pro and Everest Forms Pro require licenses and are configured as such in the production admin. Access to corresponding license numbers can be provided on request.
