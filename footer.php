<footer class="bg-light pt-5 pb-5">
  <div class="container">
    <div class="row">
      <div class="col-sm">
        <?php  wp_nav_menu( array(
          'theme_location'  => 'footer-menu',
          'depth'	          => 1,
          'container'       => 'div',
          'container_id'    => 'navbarSupportedContent',
          'menu_class'      => 'navbar-nav flex-grow-1 justify-content-end align-items-center align-items-lg-start',
          'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
          'walker'          => new WP_Bootstrap_Navwalker(),
        )); ?>
      </div>
      <div class="col-sm text-lg-right">
        <div class="d-flex justify-content-center justify-content-lg-end">
          <a href="https://twitter.com/CircleConslt" class="ml-2 mr-2">
            <svg class='remix'>
              <use xlink:href="<?php bloginfo('template_directory'); ?>/public/assets/icons/remixicon.symbol.svg#remixicon-twitter-fill"></use>
            </svg>
          </a>
          <a href="https://www.linkedin.com/company/circle-consulting-uk/about/" class="ml-2 mr-2">
            <svg class='remix'>
              <use xlink:href="<?php bloginfo('template_directory'); ?>/public/assets/icons/remixicon.symbol.svg#remixicon-linkedin-box-fill"></use>
            </svg>
          </a>
          <a href="https://www.instagram.com/circle.consulting/" class="ml-2 mr-2">
            <svg class='remix'>
              <use xlink:href="<?php bloginfo('template_directory'); ?>/public/assets/icons/remixicon.symbol.svg#remixicon-instagram-fill"></use>
            </svg>
          </a>

        </div>
      </div>
    </div>
  </div>
</footer>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<!--
SVG Injector - used for injecting inline SVGs
https://github.com/Remix-Design/remixicon#usage
-->
<script src="<?php bloginfo('template_directory'); ?>/public/assets/js/svg-injector.min.js"></script>

<script>

  var mySVGsToInject = document.querySelectorAll('.iconic-sprite');
  SVGInjector(mySVGsToInject);

</script>

<?php wp_footer(); ?>

<script src="https://unpkg.com/jarallax@1/dist/jarallax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.2.0/jquery.fitvids.min.js" charset="utf-8"></script>

<script src="" charset="utf-8"></script>

<script>
(function($) {
    $(window).load(function () {
        'use strict';

        /* Preloader */
        $('iframe').parent().fitVids();

        jarallax(document.querySelectorAll('.jarallax'), {
          speed: 0.2
        });

        $('.carousel').carousel();

    }); /* END WIDNOW LOAD */
}(jQuery));

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-147617791-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-147617791-1');
</script>


</body>
</html>
