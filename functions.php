<?php

  // Enables control of titles by Yoast SEO
  add_theme_support( 'title-tag' );

  // Register Custom Navigation Walker
  require_once get_template_directory() . '/functions/class-wp-bootstrap-navwalker.php';

  function register_menus() {
    register_nav_menu('main-menu',__( 'Main Menu' ));
    register_nav_menu('footer-menu',__( 'Footer Menu' ));
  }
  add_action( 'init', 'register_menus' );

  // Change Everest Form Plugin label in admin area
  function rename_plugin_menu() {
      global $menu;
      foreach ($menu as $key=>$item) {
        // var_dump($key);
        if ($item[0] == 'Everest Forms') {
           $menu[$key][0] = 'Forms';
        }
      }
  }
  add_action( 'admin_menu', 'rename_plugin_menu' );


  // Add custom image sizes
  add_theme_support( 'post-thumbnails' );
  add_image_size( 'team-member', 300, 300, true );
  add_image_size( 'grid_panel', 800, 800, true );
  add_image_size( 'card-image', 800, 500, true );

  /**
   * Register Blocks
   * @link https://www.billerickson.net/building-gutenberg-block-acf/#register-block
   *
   */
  function be_register_blocks() {

  	if( ! function_exists( 'acf_register_block_type' ) )
  		return;

  	acf_register_block_type( array(
  		'name'			=> 'Full Width Banner',
  		'title'			=> __( 'Full Width Banner', 'full-width-banner' ),
  		'render_template'	=> 'includes/full-width-banner.php',
      'enqueue_style' => get_stylesheet_directory_uri() . '/includes/full-width-banner.css',
      'enqueue_style' => get_stylesheet_directory_uri() . '/public/assets/css/style.css',
  		'category'		=> 'formatting',
  		'icon'			=> 'editor-table',
  		'mode'			=> 'preview',
  		'keywords'		=> array( 'banner')
  	));

  }
  add_action('acf/init', 'be_register_blocks' );

// Custom shortcode for adding a button to content

function button_shortcode( $atts, $content = null ) {
	$a = shortcode_atts( array(
		'text' => '',
		'link' => '',
	), $atts );
  return "<a href='{$a["link"]}' class='btn btn-light btn-lg'>{$a["text"]}</a>";
}
add_shortcode( 'button', 'button_shortcode' );

// Post Pagination
function pagination($pages = '', $range = 4) {

    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
        echo "</div>\n";
    }
}
