const gulp = require("gulp");
const gulpSass = require("gulp-sass");

// CSS task to convert SASS to CSS
gulp.task("css", (done) => {
  gulp
    .src("./scss/style.scss")
    .pipe(gulpSass())
    .pipe(gulp.dest("./public/assets/css/"));
  done();
});

// JS task to move bootstrap js to public dir
gulp.task("copy-js", (done) => {
  gulp
    .src("./node_modules/bootstrap/dist/js/bootstrap.min.js")
    .pipe(gulp.dest("./public/assets/js/"));
  done();
});

// Watch changes to theme SCSS
gulp.task("watch", (done) => {
  gulp.watch("./scss/*", gulp.series("css"));
  done();
});

gulp.task("default", gulp.series("watch"));
