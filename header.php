<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:image" content="<?php echo get_bloginfo('template_directory'); ?>/public/assets/images/circle-social.jpg" />
  <title><?php wp_title(''); ?></title>
  <link href="<?php echo get_bloginfo('template_directory'); ?>/style.css?v=2" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Assistant:wght@400;700&family=Inter:wght@300;700;900&display=swap" rel="stylesheet">
  <?php wp_head(); ?>
</head>

<body>
  <div class="container-fluid p-0">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light justify-between flex-grow-1 text-uppercase pl-0 pr-0 pt-4 --main-nav">
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
          <img src="<?php bloginfo('template_directory'); ?>/public/assets/images/circle-logo-horiz.png" alt="" class="" srcset="<?php bloginfo('template_directory'); ?>/public/assets/images/circle-logo-horiz@2x.png 2x">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <?php  wp_nav_menu( array(
          'theme_location'  => 'main-menu',
          'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
          'container'       => 'div',
          'container_class' => 'collapse navbar-collapse order-lg-3',
          'container_id'    => 'navbarSupportedContent',
          'menu_class'      => 'navbar-nav flex-grow-1 justify-content-end',
          'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
          'walker'          => new WP_Bootstrap_Navwalker(),
        )); ?>
          <a class="nav-item d-flex align-items-center justify-content-center pt-4 pt-md-0 --call-prompt" href="tel:01225 685 888"><svg class="mr-2" viewBox="0 0 281 281" width="20px" height="20px" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path fill="none" d="M0 0h280.555v280.555H0z"/><clipPath id="a"><path d="M0 0h280.555v280.555H0z"/></clipPath><g clip-path="url(#a)"><path d="M141.277.706c77.422 0 140.278 62.857 140.278 140.278 0 77.421-62.856 140.277-140.278 140.277C63.856 281.261 1 218.405 1 140.984 1 63.563 63.856.706 141.277.706zm73.959 176.598a4.125 4.125 0 0 0-3.69-4.093 99.33 99.33 0 0 1-4.519-.525 114.238 114.238 0 0 1-32.869-9.902 3.808 3.808 0 0 0-4.708 1.224l-12.655 17.7a107.247 107.247 0 0 1-56.238-56.238l17.733-12.671a3.744 3.744 0 0 0 1.208-4.659 114.212 114.212 0 0 1-9.926-32.877c-.165-1.142-.337-2.646-.526-4.536a4.124 4.124 0 0 0-4.092-3.697H75.898c-4.293-.002-7.9 3.36-8.201 7.642-.255 3.59-.378 6.524-.378 8.792 0 72.615 58.859 131.474 131.474 131.474 2.268 0 5.202-.131 8.793-.378 4.282-.3 7.643-3.908 7.641-8.201v-29.055h.009z" fill="#999"/></g></svg>01225 685 888</a>
          <a class="btn btn-primary nav-item d-none d-lg-block order-lg-3" href="<?php echo site_url(); ?>/jobs">Jobs</a>
          <a class="btn btn-info nav-item d-none d-lg-block order-lg-4" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
      </nav>
    </div>
  </div>
