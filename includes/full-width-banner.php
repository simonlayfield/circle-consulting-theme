<?php

$text = get_field( 'text' );
$image = get_field( 'image' );
$color = get_field( 'color' );
$link = get_field( 'link' );
$linkText = get_field( 'link_text' );

echo '<div class="row d-flex pt-5 pb-5 --homepage-banner" style="background-image: url(' . $image . ');">';
  echo '<div class="container d-flex flex-column justify-content-center">';
    echo '<div class="row justify-content-center text-center">';
      echo '<div class="col-sm-10">';
        echo '<h2 style="color:' . $color . '">' . $text . '</h2>';
      echo '</div>';
    echo '</div>';
    if (!empty(($link))) {
      echo '<div class="row mt-5 justify-content-center --banner-search">';
        echo '<div class="col-sm text-center">';
          echo '<a class="btn btn-primary btn-lg" href="' . $link . '">' . $linkText . '</a>';
        echo '</div>';
      echo '</div>';
    }
  echo '</div>';
echo '</div>';
