<?php get_header(); ?>
<div class="container-fluid p-0">
  <div class="container">
    <div class="row">
      <nav class="navbar navbar-expand-lg navbar-light justify-between flex-grow-1 text-uppercase pl-0 pr-0 pt-4 pb-4 --main-nav">
        <a class="navbar-brand" href="#">
          <img src="./assets/images/circle-logo-horiz.png" alt="" class="mb-3 mb-lg-0" srcset="./assets/images/circle-logo-horiz@2x.png 2x">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav flex-grow-1 justify-content-end">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Services
              </a>
              <div class="dropdown-menu shadow" aria-labelledby="navbarDropdown">
                <a class="dropdown-item p-3 pl-4 pr-4" href="#">Recruitment</a>
                <a class="dropdown-item p-3 pl-4 pr-4" href="#">People & Culture</a>
                <a class="dropdown-item p-3 pl-4 pr-4" href="#">Social Impact</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Social Impact</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a class="btn btn-primary" href="<?php echo site_url(); ?>/jobs">Jobs</a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a class="btn btn-info" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
            </li>
          </ul>

          <?php  wp_nav_menu( array(
          	'theme_location'  => 'primary',
          	'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
          	'container'       => 'div',
          	'container_class' => 'collapse navbar-collapse',
          	'container_id'    => 'bs-example-navbar-collapse-1',
          	'menu_class'      => 'navbar-nav mr-auto',
          	'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
          	'walker'          => new WP_Bootstrap_Navwalker(),
          )); ?>

        </div>

      </nav>
    </div>
  </div>
</div>

<div class="d-lg-none p-2">
  <a class="btn btn-primary d-block mt-2 mb-2" href="<?php echo site_url(); ?>/jobs">Jobs</a>
  <a class="btn btn-info d-block mt-2 mb-2" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
</div>

<div class="container-fluid d-flex pt-5 pb-5 --homepage-banner">
  <div class="container d-flex flex-column justify-content-center">
    <div class="row justify-content-center text-center">
      <div class="col-sm-10">
        <h2>We find talented people <strong>jobs they'll love</strong>, and help businesses create <strong>amazing places to work</strong>.</h2>
      </div>
    </div>
    <div class="row mt-5 justify-content-center --banner-search">
      <div class="col-lg-8 d-flex flex-column flex-md-row justify-content-center">
        <input type="text" class="flex-grow-1 mb-3 mb-md-0" placeholder="e.g. Associate Director"><div class="btn btn-primary d-flex flex-column justify-content-center p-3">Search Jobs</div>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row mt-4 --panels">
    <div class="col-lg d-flex justify-content-end --panel --candidates">
      <div class="d-flex flex-column justify-content-center text-center">
        <h3 class="mb-0">For Candidates</h3>
        <div class="mt-4">We source workplaces where employees feel supported, challenged, and are empowered to reach their full potential.</div>
        <div class="mt-4">
          <a href="#" class="btn btn-light">View Jobs</a>
        </div>
      </div>
    </div>
    <div class="col-lg d-flex --panel --clients">
      <div class="d-flex flex-column justify-content-center text-center">
        <h3 class="mb-0">For Clients</h3>
        <div class="mt-4">We partner with you to make sure you get the right person in the right job through effective interviewing and on-boarding.</div>
        <div class="mt-4">
          <a href="#" class="btn btn-light">Work with us</a>
        </div>
      </div>
    </div>
  </div>

  <div class="row --spaced">
    <div class="col-md text-center mt-5 mb-5 --feature">
      <img src="./assets/images/circle-services-icon-recruitment.svg" alt="">
      <h2 class="h4 mt-4 mb-4">Recruitment</h2>
      <p>We partner with you to make sure you get the right person in the right job.</p>
      <a href="#" class="btn btn-tertiary mt-3">Read more</a>
    </div>
    <div class="col-md text-center mt-5 mb-5 --feature">
      <img src="./assets/images/circle-services-icon-culture.svg" alt="">
      <h2 class="h4 mt-4 mb-4">People & Culture</h2>
      <p>We can help ensure your people receive the right training to suit their needs and the business.</p>
      <a href="#" class="btn btn-tertiary mt-3">Read more</a>
    </div>
    <div class="col-md text-center mt-5 mb-5 --feature">
      <img src="./assets/images/circle-services-icon-impact.svg" alt="">
      <h2 class="h4 mt-4 mb-4">Social Impact</h2>
      <p>We work with you to get the whole team engaged, fulfilling their potential and enjoying better working lives.</p>
      <a href="#" class="btn btn-tertiary mt-3">Read more</a>
    </div>
  </div>

  <h2 class="h4 --title-separator"><span>Our Specialist Team</span></h2>

  <div class="row justify-content-center">
    <div class="col-md-4 text-center mt-5 mb-5">
      <img src="./assets/images/elle.jpg" alt="" class="--pin-image">
      <h2 class="h5 mt-4 mb-4 font-weight-light">Elle Chappell</h2>
      <p>Director and Head of Recruitment</p>
    </div>
    <div class="col-md-4 text-center mt-5 mb-5">
      <img src="./assets/images/katie.jpg" alt="" class="--pin-image">
      <h2 class="h5 mt-4 mb-4 font-weight-light">Katie Clinton CPsychol AFBPsS</h2>
      <p>Associate People and Culture Consultant</p>
    </div>
  </div>

  <div class="row justify-content-center mb-5">
    <a href="#" class="btn btn-tertiary mt-3">About the Circle Team</a>
  </div>

</div>

<footer class="bg-light pt-5 pb-5">
  <div class="container">
    <div class="row">
      <div class="col-sm">
        <ul class="nav <flex-row></flex-row>">
          <li class="nav-item">
            <a class="nav-link active" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url(); ?>/jobs">Jobs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Privacy Policy</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact Us</a>
          </li>
        </ul>
      </div>
      <div class="col-sm text-right">
        <div class="d-flex justify-content-end">
          <a href="" class="ml-2 mr-2">
            <svg class='remix'>
              <use xlink:href="./assets/icons/remixicon.symbol.svg#remixicon-twitter-fill"></use>
            </svg>
          </a>
          <a href="" class="ml-2 mr-2">
            <svg class='remix'>
              <use xlink:href="./assets/icons/remixicon.symbol.svg#remixicon-linkedin-box-fill"></use>
            </svg>
          </a>
          <a href="" class="ml-2 mr-2">
            <svg class='remix'>
              <use xlink:href="./assets/icons/remixicon.symbol.svg#remixicon-instagram-fill"></use>
            </svg>
          </a>

        </div>
      </div>
    </div>
  </div>
</footer>
<?php get_footer(); ?>
