<?php
/**
 * Filter in `[jobs]` shortcode for job types.
 *
 * This template can be overridden by copying it to yourtheme/job_manager/job-filter-job-types.php.
 *
 * @see         https://wpjobmanager.com/document/template-overrides/
 * @author      Automattic
 * @package     wp-job-manager
 * @category    Template
 * @version     1.31.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php if ( ! is_tax( 'job_listing_type' ) && empty( $job_types ) ) : ?>
	<ul class="job_types">
		<?php foreach ( get_job_listing_types() as $type ) : ?>
			<li><label for="job_type_<?php echo esc_attr( $type->slug ); ?>" class="<?php echo esc_attr( sanitize_title( $type->name ) ); ?>"><input type="checkbox" name="filter_job_type[]" value="<?php echo esc_attr( $type->slug ); ?>" <?php checked( in_array( $type->slug, $selected_job_types ), true ); ?> id="job_type_<?php echo esc_attr( $type->slug ); ?>" /><div class="custom-check"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path fill="none" d="M0 0h24v24H0z"/><path class="custom-check-fill" d="M4 3h16c.549 0 1 .451 1 1v16c0 .549-.451 1-1 1H4c-.549 0-1-.451-1-1V4c0-.549.451-1 1-1zm7.003 13l7.07-7.071-1.414-1.414-5.656 5.657-2.829-2.829-1.414 1.414L11.003 16z" fill-rule="nonzero"/><path class="custom-check-stroke" d="M4 3h16c.549 0 1 .451 1 1v16c0 .549-.451 1-1 1H4c-.549 0-1-.451-1-1V4c0-.549.451-1 1-1z" fill="none" stroke="#677E85"/></svg></div><?php echo esc_html( $type->name ); ?></label></li>
		<?php endforeach; ?>
	</ul>
	<input type="hidden" name="filter_job_type[]" value="" />
<?php elseif ( $job_types ) : ?>
	<?php foreach ( $job_types as $job_type ) : ?>
		<input type="hidden" name="filter_job_type[]" value="<?php echo esc_attr( sanitize_title( $job_type ) ); ?>" />
	<?php endforeach; ?>
<?php endif; ?>
