<?php get_header(); ?>

<div class="d-lg-none p-2">
  <a class="btn btn-tertiary d-block mt-2 mb-2" href="<?php echo site_url(); ?>/jobs">Jobs</a>
  <a class="btn btn-info d-block mt-2 mb-2" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
</div>

<div class="container-fluid d-flex pt-5 pb-5 --homepage-banner --page">
  <div class="container d-flex flex-column justify-content-center">
    <div class="row justify-content-center text-center">
      <div class="col-sm-10">
        <h2>About</h2>
      </div>
    </div>
  </div>
</div>

<div class="container">
    <?php
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();

            the_content();

        endwhile;
    endif;
    ?>

    <?php

    $aboutPage = get_page_by_title('About Circle Consulting');

    $args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $aboutPage->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
    );

    $parent = new WP_Query( $args );

    if ( $parent->have_posts() ) : ?>

      <h2 class="h4 --title-separator"><span>Our Specialist Team</span></h2>
      <div class="row mt-5 mb-5">

        <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

          <?php if (get_field('team_member_company') == 'Circle') { ?>

            <div class="team-member col-sm-6 col-md-4 mb-5" id="parent-<?php the_ID(); ?>">

              <?php
                $image = get_field('team_member_photo');
              ?>

              <a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_image_url( $image, 'team-member', true); ?>" alt="" class="profile-image"></a>

              <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
              <h4 class="position"><?php the_field('team_member_position'); ?></h4>

            </div>

          <?php } ?>

        <?php endwhile; ?>
      </div>

      <!-- The below section is for partners - used for Cleartrack originally but saved here for future use -->

      <div class="row mt-5 mb-5">

        <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

          <?php if (get_field('team_member_company') == 'Cleartrack') { ?>

            <div class="team-member col-sm-6 col-md-4 mb-5" id="parent-<?php the_ID(); ?>">

              <?php
                $image = get_field('team_member_photo');
              ?>

              <a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_image_url( $image, 'team-member', true); ?>" alt="" class="profile-image"></a>

              <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
              <h4 class="position"><?php the_field('team_member_position'); ?></h4>

            </div>

          <?php } ?>

        <?php endwhile; ?>
      </div>


    <?php endif; wp_reset_postdata(); ?>

</div>

<?php get_footer(); ?>
