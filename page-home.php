<?php
  get_header();
?>

<div class="d-lg-none p-2">
  <a class="btn btn-tertiary d-block mt-2 mb-2" href="<?php echo site_url(); ?>/jobs">Jobs</a>
  <a class="btn btn-info d-block mt-2 mb-2" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
</div>

<?php

  // Check for a banner images added to ACF field
  $homepageBannerImage = get_field('banner_background_image');
  $homepageSecondaryBannerImage = get_field('secondary_banner_background_image');

?>

<div class="container-fluid d-flex pt-5 pb-5 --homepage-banner"
  style="<?php if ($homepageBannerImage) { echo 'background-image: url(' .  $homepageBannerImage["sizes"]["large"] . ');'; } ?>"
  >
  <div class="container d-flex flex-column justify-content-center">

    <div class="row justify-content-center text-center">
      <div class="col-sm-12">

        <?php
          $banner_fields = get_field('homepage_banner');
        ?>

        <div class="homepage-banner__title">
          <?php echo $banner_fields['banner_text']; ?>
        </div>

      </div>
    </div>

    <div class="row mt-4 justify-content-center">
     
      <div class="col-lg-7">
        <div class="row">

          <div class="col-sm-6">
            <div class="p-md-2 mb-4 mb-sm-0">
              <a href="<?php echo site_url(); ?>/recruitment" class="btn btn-primary btn-lg btn-block">I'm looking to hire</a>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="p-md-2">
              <a href="<?php echo site_url(); ?>/jobs" class="btn btn-secondary btn-lg btn-block">I'm looking for a job</a>
            </div>
          </div>

        </div>
      </div>

    </div>

    <?php if ($banner_fields['banner_secondary_text']) : ?>

      <div class="row mt-4 justify-content-center text-center">
        <div class="col-sm-12">

          <div class="homepage-banner__title">
            <?php echo $banner_fields['banner_secondary_text']; ?>
          </div>

        </div>
      </div>

    <?php endif; ?>

  </div>
</div>

<div class="container-fluid pt-5 pb-5 --homepage-banner-tiles jarallax"
  style="<?php if ($homepageSecondaryBannerImage) { echo 'background-image: url(' .  $homepageSecondaryBannerImage["sizes"]["large"] . ');'; } ?>"
  >
  <div class="container">
    <div class="row justify-content-center text-center">

      <?php

        if (have_rows('homepage_banner_tiles_tiles')) :

          while (have_rows('homepage_banner_tiles_tiles')) : the_row();

      ?>

            <div class="circle-banner-tile" style="background-color: <?php echo get_sub_field('background_colour'); ?>; color: <?php echo get_sub_field('text_colour'); ?>;">

              <?php

                $title = get_sub_field('title');
                $cta = get_sub_field('cta');

              ?>

              <div class="circle-banner-tile__title">
                <h2><?php echo $title; ?></h2>
              </div>

              <div class="circle-banner-tile__cta" style="color: <?php echo get_sub_field('background_colour'); ?>">
                <a href="<?php echo $cta['url']; ?>" class="btn btn-block"><?php echo $cta['text']; ?></a>
              </div>

            </div>

      <?php endwhile; ?>
      <?php endif; ?>

    </div>

  </div>
</div>

<?php

  if( have_rows('homepage_grid_rows_rows') ):

    echo '<div class="container mt-4">';

      while ( have_rows('homepage_grid_rows_rows') ) : the_row();

      echo '<div class="circle-grid-panel__intro text-center">' . get_sub_field('intro_text') . '</div>';
      echo '<div class="row my-3 my-md-5">';

      if( have_rows('panels') ):

          while ( have_rows('panels') ) : the_row();

          $panel_image = get_sub_field('image');

          ?>

            <div class="circle-grid-panel col-sm p-5 text-center d-flex align-items-center justify-content-center"
                  style="background-color: <?php the_sub_field('background_colour'); ?>; color: <?php the_sub_field('text_colour'); ?>; background-image: url('<?php echo $panel_image['sizes']['grid_panel']; ?>')">
              <div class="w-100"><?php the_sub_field('text'); ?></div>

            </div>

          <?php

            endwhile;

            echo '</div>';

        endif;

      endwhile;

    echo '</div>';

  endif;

?>

<?php

 if (have_rows('testimonials')) : ?>
   <div class="container mt-5">
     <h2 class="h4 --title-separator"><span>What are people saying?</span></h2>

   <div class="d-flex">

     <div id="homepage-carousel" class="carousel slide d-flex" data-ride="carousel">

       <div class="carousel__link carousel__link--left">
         <a href="#carouselExampleControls" role="button" data-slide="prev" class="carousel__arrow carousel__arrow--left">

           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M10.828 12l4.95 4.95-1.414 1.414L8 12l6.364-6.364 1.414 1.414z"/></svg>


           <span class="sr-only">Previous</span>
         </a>

       </div>

       <div class="carousel-inner">

         <?php while (have_rows('testimonials')) : the_row() ?>

           <div class="carousel-item w-100 justify-content-center <?php echo get_row_index() === 1 ? 'active' : ''; ?>">

             <div class="text-center mt-3 mb-3 --feature">
               <img src="<?php bloginfo('template_directory'); ?>/public/assets/images/circle-quotations.svg" alt="Testimonial Icon" width="100" class="mb-3">
               <?php echo get_sub_field('quote'); ?>
             </div>

           </div>

         <?php endwhile; ?>

       </div>

       <div class="carousel__link">
         <a href="#carouselExampleControls" role="button" data-slide="next" class="carousel__arrow carousel__arrow--right">
           <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z"/></svg>
           <span class="sr-only">Next</span>
         </a>

       </div>

       </div>
     </div>

     <div class="row mb-5 justify-content-center">

     <?php

        $ctaButton = get_field('cta_button');
        $buttonColour = $ctaButton['button_colour'];
        $buttonText = $ctaButton['button_text'];
        $buttonUrl = $ctaButton['button_url'];

      ?>

     <div class="col-10 text-center mt-5 mb-5">
       <a href="<?php echo $buttonUrl ? $buttonUrl : site_url() . "/contact"; ?>" class="btn btn-secondary btn-lg mt-3"
         style="<?php if ($buttonColour) { echo 'background-color: ' . $buttonColour . ';'; } ?>"
         >
         <?php echo $buttonText ? $buttonText : "Get in touch with Circle"; ?>
       </a>
     </div>

     </div>

   </div>
<?php endif; ?>

<?php

  $aboutPage = get_page_by_title('About Circle Consulting');

  $args = array(
  'post_type'      => 'page',
  'posts_per_page' => -1,
  'post_parent'    => $aboutPage->ID,
  'order'          => 'ASC',
  'orderby'        => 'menu_order'
  );

  $parent = new WP_Query( $args );

  if ( $parent->have_posts() ) : ?>

    <?php

      $homepage_team_section = get_field('homepage_team_section');
      $team_section_bg = $homepage_team_section['background_colour'];

    ?>

  <div class="bg-row" style="background-color: <?php echo $team_section_bg ? $team_section_bg : '#600f9a'; ?>">

    <div class="container">

      <h2 class="h4 mt-5 text-center text-white">Our Specialist Team</h2>
      <div class="row pt-5 pb-5 justify-content-center text-white">

        <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

          <?php if (get_field('team_member_company') == 'Circle') { ?>

            <div class="team-member col-sm-6 col-md-4 mb-5" id="parent-<?php the_ID(); ?>">

              <?php
                $image = get_field('team_member_photo');
              ?>

              <a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_image_url( $image, 'team-member', true); ?>" alt="" class="profile-image"></a>

              <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
              <div class="position"><?php the_field('team_member_position'); ?></div>
              <div class="cta"><a href="<?php the_permalink(); ?>" class="btn btn-light">Get to know</a></div>

            </div>

          <?php } ?>

        <?php endwhile; ?>
      </div>

    </div>

  </div>

<?php endif; wp_reset_postdata(); ?>

<?php get_footer(); ?>
