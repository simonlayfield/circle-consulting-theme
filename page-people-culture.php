
<?php get_header(); ?>

<div class="d-lg-none p-2">
  <a class="btn btn-tertiary d-block mt-2 mb-2" href="<?php echo site_url(); ?>/jobs">Jobs</a>
  <a class="btn btn-info d-block mt-2 mb-2" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
</div>

<div class="container-fluid d-flex pt-5 pb-5 --homepage-banner --page">
  <div class="container d-flex flex-column justify-content-center">
    <div class="row justify-content-center text-center">
      <div class="col-sm-10">
        <h2><?php the_title(); ?></h2>
      </div>
    </div>
  </div>
</div>

<div class="container --content">

  <div class="row mb-5 mt-5">
    <div class="col-sm">
      We don't just find you talented people, we help develop them too. People are the engine rooms of any business; create an amazing place to work and develop the right skills and behaviours and your people will take your business all the way.
    </div>
  </div>

  <div class="row">
    <div class="col-sm text-center">
      <img src="<?php bloginfo('template_directory'); ?>/public/assets/images/people-culture-diagram.png" alt="" class="diagram">
    </div>
  </div>

  <div class="row">
    <div class="col-sm-4">
      <h4 class="h4 mb-4 mt-4 --title-bt -green">Leadership</h4>
      <p>Ensuring your business is able to identify, develop and leverage exceptional leadership in order to get the best from your people. Leaders who are future fit, able to motivate their teams and provide clarity and direction in a fast paced and ever-changing world.</p>
    </div>
    <div class="col-sm-4">
      <h4 class="h4 mb-4 mt-4 --title-bt -purple">Team Development</h4>
      <p>We help your teams become high performing teams, driven by a common purpose, able to communicate, collaborate and excel through collective ways of thinking and operating.</p>
    </div>
    <div class="col-sm-4">
      <h4 class="h4 mb-4 mt-4 --title-bt -mauve">Culture</h4>
      <p>Culture eats strategy for breakfast. Perhaps culture is your strategy! Either way, we can support you in developing the values and behaviours that can help create an amazing place to work, and ultimately, a winning organisation.</p>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-sm-4">
      <h3 class="h4 mb-4 mt-4 --title-bt -yellow">Strategy Alignment</h3>
      <p>Yes, we know what we said about culture and breakfast, but we also recognise that supporting business leaders in creating clarity and direction is actually quite important too. Building that compelling vision and purpose and creating a motivating story around how your people fit in to that master plan, means you’re more likely to have everyone aligned and creating success together.</p>
    </div>

    <div class="col-sm-4">
      <h3 class="h4 mb-4 mt-4 --title-bt -grey">Resilience and Well-being</h3>
      <p>The world of work is now more connected, complicated and complex than ever before. We support people in developing the skills and strategies they need in order to come out on top, feeling fulfilled and in control.</p>
    </div>
    <div class="col-sm-4">
      <h3 class="h4 mb-4 mt-4 --title-bt -paleblue">Coaching</h3>
      <p>One-on-one private coaching, at any level from Chief Executive Officer to Chief Tea Maker. If you’re trying to reach new heights and need a little support on the way, our coaches can provide the right environment and challenge you need to get there.</p>
    </div>

  </div>


</div>

<?php get_footer(); ?>
