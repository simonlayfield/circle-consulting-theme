<?php get_header(); ?>

<div class="d-lg-none p-2">
  <a class="btn btn-tertiary d-block mt-2 mb-2" href="<?php echo site_url(); ?>/jobs">Jobs</a>
  <a class="btn btn-info d-block mt-2 mb-2" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
</div>


<div class="container-fluid d-flex pt-5 pb-5 --homepage-banner --page">
  <div class="container d-flex flex-column justify-content-center">
    <div class="row justify-content-center text-center">
      <div class="col-sm-10">
        <h2>Resources</h2>
      </div>
    </div>
  </div>
</div>

  <?php
    if (have_posts()) :

      echo '<div class="container mt-5 mb-4">';

        while (have_posts()) : the_post();

          the_content();

        endwhile;

      echo '</div>';

    endif;
  ?>

<div class="container mt-5 mb-5">
  <div class="row">

    <?php
      $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
      $args = array (
        'posts_per_page' => 9,
        'paged' => $paged,
        'orderby' => 'date'
      );

      $cat_posts = new WP_query($args);

      if ($cat_posts->have_posts()) : while ($cat_posts->have_posts()) : $cat_posts->the_post();

      $categories = get_the_category();
      $cat_name = $categories[0]->cat_name;
      $cat_colour = get_field('category_colour', 'term_' . $categories[0]->term_id);

      ?>

      <div class="col-sm-6 col-md-4 --resource-card <?php echo '--resource-card-' . strtolower($cat_name); ?>">
        <div class="category-card" style="background-color: <?php echo $cat_colour ? $cat_colour : ''?>;">
          <div class="category-image">
            <div class="category-label"><?php echo $cat_name; ?></div>
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('card-image'); ?></a>
          </div>
          <div class="--card-body p-4">
            <div class="h5"><?php the_title(); ?></div>
            <?php the_excerpt(); ?>
            <div class="category-cta">
              <a href="<?php the_permalink(); ?>" class="btn btn-light btn-block">
                View
              </a>
            </div>
          </div>
        </div>
      </div>

      <?php

      endwhile;

    endif;

    ?>

  </div>

  <div class="circle-pagination">
    <?php
      if (function_exists("pagination")) {
        pagination($cat_posts->max_num_pages);
      }
    ?>
  </div>

</div>

<?php get_footer(); ?>
