<?php
/**
* Template Name: Team Member
*/

 get_header(); ?>

<div class="d-lg-none p-2">
  <a class="btn btn-tertiary d-block mt-2 mb-2" href="<?php echo site_url(); ?>/jobs">Jobs</a>
  <a class="btn btn-info d-block mt-2 mb-2" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
</div>

<div class="container-fluid d-flex pt-5 pb-5 --homepage-banner --page">
  <div class="container d-flex flex-column justify-content-center">
    <div class="row justify-content-center text-center">
      <div class="col-sm-10">
        <div class="h2">

          <?php

            if (get_field('team_member_company') == 'Circle') {
              echo 'The Circle Consulting Team';
            } else {
              echo 'Our Cleartrack Partners';
            }

          ?>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="container --content team-profile">
  <?php
  if ( have_posts() ) :
      while ( have_posts() ) : the_post();
        $image = get_field('team_member_photo');
        $img_src = wp_get_attachment_image_url( $image, 'team-member' );
        $img_srcset = wp_get_attachment_image_srcset( $image, 'team-member' );

        if ($image) { ?>
          <div class="image mb-4">
              <img src="<?php echo esc_url( $img_src ); ?>"
                   srcset="<?php echo esc_attr( $img_srcset ); ?>"
                   sizes="(min-width: 760px) 15vw, (min-width: 500px) 30vw, 50vw" alt="" class="profile-image">
          </div>
          <div class="position">
            <h1 class="h4 mb-3"><?php the_title(); ?></h1>
            <div class="h5 mb-5"><?php echo get_field('team_member_position'); ?></div>
          </div>
        <?php } ?>
      <div class="bio">
        <?php the_content(); ?>
      </div>

      <?php endwhile;
  endif;
  ?>
</div>

<?php get_footer(); ?>
