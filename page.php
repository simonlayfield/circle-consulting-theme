<?php get_header(); ?>

<div class="d-lg-none p-2">
  <a class="btn btn-tertiary d-block mt-2 mb-2" href="<?php echo site_url(); ?>/jobs">Jobs</a>
  <a class="btn btn-info d-block mt-2 mb-2" href="<?php echo site_url(); ?>/submit-your-cv">Submit CV</a>
</div>

<div class="container-fluid d-flex pt-5 pb-5 --homepage-banner --page">
  <div class="container d-flex flex-column justify-content-center">
    <div class="row justify-content-center text-center">
      <div class="col-sm-10">
        <h2><?php the_title(); ?></h2>
      </div>
    </div>
  </div>
</div>

<div class="container --content">
  <?php
  if ( have_posts() ) :
      while ( have_posts() ) : the_post();
          the_content();
      endwhile;
  endif;
  ?>
</div>

<?php get_footer(); ?>
