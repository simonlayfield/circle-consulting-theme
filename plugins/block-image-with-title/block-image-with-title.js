wp.blocks.registerBlockType('circle/image-with-title', {
  title: "Image with Title",
  icon: "editor-textcolor",
  category: "common",
  attributes: {
    title: {
      type: "string"
    }
  },
  edit: (props) => {

    function updateTitle(value) {
      props.setAttributes({
        image: value
      })
    }

    return wp.element.createElement("div", null, wp.element.createElement(wp.components.FormFileUpload, {
      image: props.attributes.image,
    }));

  },
  save: () => {
    return null
  }
});
