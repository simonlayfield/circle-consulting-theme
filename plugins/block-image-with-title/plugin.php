<?php
/**
 * Plugin Name: Block - Image with Title
 * Author: Circle
 * Version: 1.0.0
 */
function loadBlockImageWithTitle() {
  wp_enqueue_script(
    'image-with-title',
    plugin_dir_url(__FILE__) . 'block-image-with-title.js',
    array('wp-blocks', 'wp-i18n', 'wp-editor'),
    true
  );
}

add_action('enqueue_block_editor_assets', 'loadBlockImageWithTitle');

function imageWithTitleOutput($props) {
  return 'Hi';
}

register_block_type( 'circle/image-with-title', array(
  'render_callback' => 'imageWithTitleOutput'
));

?>
