wp.blocks.registerBlockType('circle/separator-title', {
  title: "Separator Title",
  icon: "editor-textcolor",
  category: "common",
  attributes: {
    title: {
      type: "string"
    }
  },
  edit: (props) => {

    function updateTitle(value) {
      props.setAttributes({
        title: value
      })
    }

    return wp.element.createElement("div", null, wp.element.createElement(wp.components.TextControl, {
      value: props.attributes.title,
      onChange: updateTitle
    }));

  },
  save: () => {
    return null
  }
});
