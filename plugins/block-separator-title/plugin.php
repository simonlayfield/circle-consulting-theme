<?php
/**
 * Plugin Name: Block - Separator Title
 * Author: Circle
 * Version: 1.0.0
 */
function loadBlockSeparatorTitle() {
  wp_enqueue_script(
    'separator-title',
    plugin_dir_url(__FILE__) . 'block-separator-title.js',
    array('wp-blocks', 'wp-i18n', 'wp-editor'),
    true
  );
}

add_action('enqueue_block_editor_assets', 'loadBlockSeparatorTitle');

function separatorTitleOutput($props) {
  return '<h2 class="h4 --title-separator text-center"><span>' . $props['title'] . '</span></h2>';
}

register_block_type( 'circle/separator-title', array(
  'render_callback' => 'separatorTitleOutput'
));

?>
