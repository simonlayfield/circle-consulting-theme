<?php get_header(); ?>

<div class="container-fluid d-flex pt-5 pb-5 --homepage-banner --page">
  <div class="container d-flex flex-column justify-content-center">
    <div class="row justify-content-center text-center">
      <div class="col-sm-10">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
</div>

  <?php
    if (have_posts()) :

      echo '<div class="container mt-5 mb-5">';

        while (have_posts()) : the_post();

          the_content();

        endwhile;

      echo '</div>';

    endif;
  ?>


</div>

<?php get_footer(); ?>
